from os import stat
from fastapi import FastAPI,status,HTTPException
from pydantic import BaseModel
from typing import Optional,List
from database import SessionLocal
import models

app=FastAPI(title='Password Management System - API', description='API to query the password management system', version='0.1')

class complexity(BaseModel): #serializer
    complexity_id:int
    uppercase:int
    lowercase: int
    symbols: int
    numbers: int

    class Config:
        orm_mode=True

db=SessionLocal()

@app.get('/complexity',response_model=List[complexity],status_code=200)
def get_complexity():
    complexity=db.query(models.complexity).all()
    return complexity































# from fastapi import FastAPI
# from typing import Optional
# from pydantic import BaseModel
#
# app = FastAPI()
#
# students = {
#     1: {
#         "name": "john",
#         "age": 17,
#         "year": "year 12"
#     }
# }
#
#
# class Student(BaseModel):
#     name: str
#     age: int
#     year: str
#
#
# class UpdateStudent(BaseModel):
#     name: Optional[str] = None
#     age: Optional[int] = None
#     year: Optional[str] = None
#
#
# @app.get("/")
# def index():
#     return {"name": "First Data"}
#
#
# @app.get("/get-student/{studnet_id}")
# def get_student(student_id: int):
#     return students[student_id]
#
#
# @app.get("/get-by-name")
# def get_student(*, student_id: int, name: Optional[str] = None, test: int):
#     for student_id in students:
#         if students[student_id]["name"] == name:
#             return students[student_id]
#         return {"Data": "Not Found"}
#
#
# @app.post("/create-student/{student_id}")
# def create_studnet(student_id: int, student: Student):
#     if student_id in students:
#         return {"ERROR": "Student Exists"}
#     students[student_id] = student
#     return students[student_id]
#
#
# @app.put("/update-student/{student_id}")
# def update_student(student_id: int, student: UpdateStudent):
#     if student_id not in students:
#         return {"Error": "Student does not exists"}
#     if student.name != None:
#         students[student_id].name = student.name
#     if student.name != None:
#         students[student_id].age = student.age
#     if student.name != None:
#         students[student_id].year = student.year
#     return students[student_id]
